var express = require("express");
var app = express();

app.listen(3000, () => {
    console.log("Servidor: 3000");
});

app.get("/api/products", (req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', 'http://localhost:4200');
    res.json([
        'Produto 1',
        'Produto 2',
        'Produto 3',
        'Produto 4'
    ]);
});